<?php


add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

function ihag_enqueue() {

	wp_enqueue_script( 'ihag-script', get_stylesheet_directory_uri() . '/ihag-script.js', array(), false, true );

}
add_action( 'wp_enqueue_scripts', 'ihag_enqueue' );